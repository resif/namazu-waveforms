#!/bin/bash

directory=$1

mkdir -p $directory

startsync=$(date --date "$(date) -1 second" --iso-8601=seconds | cut -d '+' -f 1)

if [ -f "$directory/.sync" ]; then
  starttime=$(cat "$directory/.sync")
else
  starttime="0000-01-01T00:00:00"
fi

updated_url="https://api.franceseisme.fr/fdsnws/event/1/query?format=json&updatedafter=$starttime"
created_url="https://api.franceseisme.fr/fdsnws/event/1/query?format=json&starttime=$starttime"

ids=$((curl -s $updated_url | jq -r '.features[]|.id' ; curl -s $created_url | jq -r '.features[]|.id') | sort -u)

for event_id in $ids; do
  echo $event_id
  #[ -f "$directory/$event_id.json" ] && continue

  waveforms2json --sample-target 6000 \
  --namazu 'https://api.franceseisme.fr' \
  --dataselect 'http://10.0.1.36:8080,http://ws.resif.fr,http://service.iris.edu' \
  $event_id "$directory/$event_id.json"
done

echo $startsync > "$directory/.sync"
