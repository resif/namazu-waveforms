"use strict";

var Waveforms = function () {
  var obj = {
    addTraceNames(layout, traces, start_time) {
      var start_time = new Date(start_time).toISOString();

      traces.map((trace, index) => {
        var axisName = index > 0 ? "y" + (index + 1) : "y";

        var name = [trace.network, trace.station].join(".");

        var distance = Math.round(trace.distance * 111);

        var legend = `Distance : ${distance}km<br>Azimuth : ${trace.azimuth}&deg;`;

        layout["annotations"].push(
          {
            x: start_time,
            y: 0,
            yref: axisName,
            text: name,
            showarrow: false,
            xanchor: "left",
            yanchor: "bottom",
            font: {
              family: "Arial",
              size: 16,
              color: "black",
            },
          },
          {
            x: start_time,
            y: 0,
            yref: axisName,
            text: legend,
            showarrow: false,
            xanchor: "left",
            yanchor: "top",
            align: "center",
            font: {
              family: "Arial",
              size: 12,
              color: "grey",
            },
          }
        );
      });

      return layout;
    },
    addPicks(layout, traces) {
      traces.map((trace, index) => {
        var axisName = index > 0 ? "y" + (index + 1) : "y";

        trace.picks.map((pick) => {
          layout["annotations"].push({
            x: new Date(Date.parse(pick.time)).toISOString(),
            y: 0,
            yref: axisName,
            text: pick.phase,
            shadowarrow: true,
            arrowhead: 5,
            ax: -30,
            ay: -30,
          });
        });
      });

      return layout;
    },
    addDomains(layout, count) {
      var offset = 1 / count;

      Array.from(Array(count).keys()).map((index) => {
        var axisName = index > 0 ? "yaxis" + (index + 1) : "yaxis";

        layout[axisName] = {
          domain: [index * offset, (index + 1) * offset],
          anchor: index > 0 ? "y" + (index + 1) : "y",
          // Create artefacts
          // autotick: false,
          // ticks: "",
          // showticklabels: false,
        };
      });

      return layout;
    },
    getEventID() {
      const queryString = window.location.search;
      const urlParams = new URLSearchParams(queryString);

      return urlParams.get("event_publicid");
    },
    plotlyTrace(trace, index, start_time) {
      var name = [trace.network, trace.station].join(".");

      return {
        name: name,
        y: trace.waveform || [],
        x: trace.waveform
          ? this.plotlyX(trace.waveform.length, start_time, trace.sampling_rate)
          : [],
        // yaxis: index > 0 ? "y" + index : "y"
        yaxis: index > 0 ? "y" + (index + 1) : "y",
      };
    },
    plotlyX(length, startTime, samplingRate) {
      var millisecondOffset = 1000 / samplingRate;

      return Array.from(Array(length).keys()).map((index) => {
        return new Date(startTime + index * millisecondOffset).toISOString();
      });
    },
    processResponse(response) {
      var start_time = Date.parse(response.data.start_time);
      var traces = response.data.data;

      var plotlyData = traces.reverse().map((trace, index) => {
        return obj.plotlyTrace(trace, index, start_time);
      });

      var element = document.getElementById("waveforms");

      var event_time = new Date(Date.parse(response.data.time));

      console.log(event_time);

      var title = `Event PublicID : ${response.data.event_publicid}<br>
        ${response.data.description.en} (${event_time.toUTCString()})<br>
        Latitude : ${response.data.latitude}&deg;, Longitude : ${
        response.data.longitude
      }&deg; Depth : ${response.data.depth}km
      `;

      var layout = {
        title: title,
        // legend: { traceorder: "reversed" },
        showlegend: false,
        height: 100 * traces.length,
        annotations: [],
        images: [
          {
            source: "images/logo-bcsf-renass.png",
            layer: "below",
            opacity: 0.1,
            x: 0.7,
            y: 0.7,
            xanchor: "right",
            yanchor: "bottom",
            xref: "paper",
            yref: "paper",
            sizex: 0.3,
            sizey: 0.3,
          },
          {
            source: "images/logo-resif.png",
            layer: "below",
            opacity: 0.1,
            x: 0.7,
            y: 0.7,
            xanchor: "right",
            yanchor: "top",
            xref: "paper",
            yref: "paper",
            sizex: 0.3,
            sizey: 0.3,
          },
        ],
      };

      layout = obj.addDomains(layout, traces.length);
      layout = obj.addPicks(layout, traces);
      layout = obj.addTraceNames(layout, traces, start_time);

      Plotly.newPlot(element, plotlyData, layout);
    },
    getWaveforms(eventPublicID) {
      return axios
        .get("/waveforms/" + eventPublicID + ".json")
        .then(this.processResponse);
    },
  };

  return obj;
};
